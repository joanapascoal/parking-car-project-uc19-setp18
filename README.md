    # parking-car-project-uc19-setp18

## Project structure to the final evaluation of Data Structures & Algorithms Unit - Computer Science Set/2018 - ETIC

# Run Project

1. Clone project
```
git clone git@gitlab.com:joanapascoal/parking-car-project-uc19-setp18.git
```

2. Open Solution on Visual Studio IDE

3. Run project
- Select ManagerSoftware.GUI
![Run Project](https://gitlab.com/joanapascoal/parking-car-project-uc19-setp18//tree/master/images/run_project.png)

- Click on Start option

# How use GUI
1. Insert Client data
![Fill Data](https://gitlab.com/joanapascoal/parking-car-project-uc19-setp18//tree/master/images/fill_data.png)

2. Get Parking number
![Get Parking Number](https://gitlab.com/joanapascoal/parking-car-project-uc19-setp18//tree/master/images/parking_number.png)

3. Get Day Info
![get Lists](https://gitlab.com/joanapascoal/parking-car-project-uc19-setp18//tree/master/images/day_info.png)

4. Close Park (Reset data)
![reset Lists](https://gitlab.com/joanapascoal/parking-car-project-uc19-setp18/tree/master/images/close_park.png)
