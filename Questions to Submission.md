# Questions to Submission

1. Quais as estruturas de dados utilizadas no Projecto? 
Exemplifica com excertos de código e explica a sua utilização.
2. Que tipo de algoritmo/s foram utilizados no projecto?
Exemplifica com excertos de código e explica a sua utilização.
3. Qual o algoritmo mais eficiente (bubbleSort ou quicksort)? Explica porquê.
4. Qual principal diferença entre uma Stack e uma Queue?
5. Explica o conceito de pré e pós-condição. Exemplifica com excertos do código.
6. Eventuais riscos de não se assegurar que uma pré-condição é cumprida? 
Dá um exemplo do que poderia acontencer no projecto.
7. Porque utilizamos encapsulamento? Vantagens? Perigos? 
Exemplifica com excertos do projecto.
8. Genericamente qual consideras ser a estrutura de dados mais vantajosa 
(das utilizadas) e porquê? Exemplifica 2 vantagens com excertos do código.