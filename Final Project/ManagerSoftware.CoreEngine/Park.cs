﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagerSoftware.CoreEngine
{
    public class Park
    {

        //Attributes

        // Here should be the information about current cars in the park, e.g. List<Car>, Queue<Car> or Stack<Car>

        public Park() {
        }

        public int InsertQueue(string name, string email, string contact, string company_name)
        {
            // To be Updated
            // Create the car
            // Insert it on correct data structure
            // Return data structure position
            return 0;
        }

        public int InsertStack(string name, string email, string contact)
        {
            // To be Updated
            // Create the car
            // Insert it on correct data structure
            // Return data structure position
            return 0;
        }

        private void CreateClient(string name, string email, string contact)
        {
            // To be Updated

        }
      
        public List<Car> OrderCars()
        {
            //Receive a car and the current list of parking cars and organize it

            return new List<Car>();
        }

        public List<Car> GetCompanyOrderCars()
        {
            // Return the list of company cars order by alphabetic company order and client name order

            // To be Updated
            return new List<Car> { new Car(DateTime.Now, 987) };
        }


        public List<Car> GetVisitorsOrderCars()
        {
            // Return the list of visitors cars order by entry datetime
            // To be Updated
            return new List<Car> { new Car(DateTime.Now, 123) };
        }


        public void Reset() { 
            // Reset the information park at the end of the day
            // To be Updated
        }
    }
}
