﻿namespace ManagerSoftware.GUI
{
    partial class Manager_Software_Windows
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.get_parking_number = new System.Windows.Forms.Button();
            this.get_day_info = new System.Windows.Forms.Button();
            this.spot_number = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.role = new System.Windows.Forms.ComboBox();
            this.contactNumber = new System.Windows.Forms.TextBox();
            this.email = new System.Windows.Forms.TextBox();
            this.name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.visitorsList = new System.Windows.Forms.ListBox();
            this.company_name = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.close_park = new System.Windows.Forms.Button();
            this.companiesList = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // get_parking_number
            // 
            this.get_parking_number.Location = new System.Drawing.Point(88, 283);
            this.get_parking_number.Margin = new System.Windows.Forms.Padding(2);
            this.get_parking_number.Name = "get_parking_number";
            this.get_parking_number.Size = new System.Drawing.Size(114, 28);
            this.get_parking_number.TabIndex = 4;
            this.get_parking_number.Text = "Get Parking Number";
            this.get_parking_number.UseVisualStyleBackColor = true;
            this.get_parking_number.Click += new System.EventHandler(this.get_park_number);
            // 
            // get_day_info
            // 
            this.get_day_info.Location = new System.Drawing.Point(363, 261);
            this.get_day_info.Margin = new System.Windows.Forms.Padding(2);
            this.get_day_info.Name = "get_day_info";
            this.get_day_info.Size = new System.Drawing.Size(98, 43);
            this.get_day_info.TabIndex = 5;
            this.get_day_info.Text = "Get Day Info";
            this.get_day_info.UseVisualStyleBackColor = true;
            this.get_day_info.Click += new System.EventHandler(this.get_day_info_Click);
            // 
            // spot_number
            // 
            this.spot_number.Enabled = false;
            this.spot_number.Location = new System.Drawing.Point(87, 61);
            this.spot_number.Margin = new System.Windows.Forms.Padding(2);
            this.spot_number.Name = "spot_number";
            this.spot_number.Size = new System.Drawing.Size(115, 20);
            this.spot_number.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Enabled = false;
            this.label1.Location = new System.Drawing.Point(65, 132);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(65, 165);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Location = new System.Drawing.Point(65, 199);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Contact";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Location = new System.Drawing.Point(65, 230);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Role";
            // 
            // role
            // 
            this.role.FormattingEnabled = true;
            this.role.Items.AddRange(new object[] {
            "Visitor",
            "Company"});
            this.role.Location = new System.Drawing.Point(146, 227);
            this.role.Margin = new System.Windows.Forms.Padding(2);
            this.role.Name = "role";
            this.role.Size = new System.Drawing.Size(92, 21);
            this.role.TabIndex = 3;
            this.role.SelectedIndexChanged += new System.EventHandler(this.textChanged);
            // 
            // contactNumber
            // 
            this.contactNumber.Location = new System.Drawing.Point(146, 193);
            this.contactNumber.Margin = new System.Windows.Forms.Padding(2);
            this.contactNumber.Name = "contactNumber";
            this.contactNumber.Size = new System.Drawing.Size(92, 20);
            this.contactNumber.TabIndex = 2;
            this.contactNumber.TextChanged += new System.EventHandler(this.textChanged);
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(146, 159);
            this.email.Margin = new System.Windows.Forms.Padding(2);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(92, 20);
            this.email.TabIndex = 1;
            this.email.TextChanged += new System.EventHandler(this.textChanged);
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(146, 128);
            this.name.Margin = new System.Windows.Forms.Padding(2);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(92, 20);
            this.name.TabIndex = 0;
            this.name.TextChanged += new System.EventHandler(this.textChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 20;
            // 
            // visitorsList
            // 
            this.visitorsList.FormattingEnabled = true;
            this.visitorsList.Location = new System.Drawing.Point(332, 161);
            this.visitorsList.Margin = new System.Windows.Forms.Padding(2);
            this.visitorsList.Name = "visitorsList";
            this.visitorsList.Size = new System.Drawing.Size(265, 82);
            this.visitorsList.TabIndex = 15;
            // 
            // company_name
            // 
            this.company_name.Location = new System.Drawing.Point(146, 259);
            this.company_name.Margin = new System.Windows.Forms.Padding(2);
            this.company_name.Name = "company_name";
            this.company_name.Size = new System.Drawing.Size(92, 20);
            this.company_name.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Enabled = false;
            this.label6.Location = new System.Drawing.Point(61, 261);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Company Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Enabled = false;
            this.label7.Location = new System.Drawing.Point(84, 46);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(139, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "YOUR PARKING NUMBER";
            // 
            // close_park
            // 
            this.close_park.Location = new System.Drawing.Point(489, 260);
            this.close_park.Margin = new System.Windows.Forms.Padding(2);
            this.close_park.Name = "close_park";
            this.close_park.Size = new System.Drawing.Size(102, 43);
            this.close_park.TabIndex = 6;
            this.close_park.Text = "Close Park";
            this.close_park.UseVisualStyleBackColor = true;
            this.close_park.Click += new System.EventHandler(this.reset_button);
            // 
            // companiesList
            // 
            this.companiesList.FormattingEnabled = true;
            this.companiesList.Location = new System.Drawing.Point(332, 46);
            this.companiesList.Margin = new System.Windows.Forms.Padding(2);
            this.companiesList.Name = "companiesList";
            this.companiesList.Size = new System.Drawing.Size(265, 82);
            this.companiesList.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Enabled = false;
            this.label8.Location = new System.Drawing.Point(329, 31);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Companies List";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Enabled = false;
            this.label9.Location = new System.Drawing.Point(329, 146);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Visitors List";
            // 
            // Manager_Software_Windows
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 314);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.companiesList);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.company_name);
            this.Controls.Add(this.visitorsList);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.close_park);
            this.Controls.Add(this.name);
            this.Controls.Add(this.email);
            this.Controls.Add(this.contactNumber);
            this.Controls.Add(this.role);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.spot_number);
            this.Controls.Add(this.get_day_info);
            this.Controls.Add(this.get_parking_number);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Manager_Software_Windows";
            this.Text = "Manager_Software_Windows";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox role;
        private System.Windows.Forms.TextBox contactNumber;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox company_name;

        private System.Windows.Forms.Button get_parking_number;
        private System.Windows.Forms.TextBox spot_number;

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;

        private System.Windows.Forms.Button get_day_info;
        private System.Windows.Forms.Button close_park;
        private System.Windows.Forms.ListBox companiesList;
        private System.Windows.Forms.ListBox visitorsList;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}