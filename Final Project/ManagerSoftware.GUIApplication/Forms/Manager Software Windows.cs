﻿using System;
using System.Windows.Forms;
using ManagerSoftware.GUIApplication;
using ManagerSoftware.CoreEngine;
using System.Collections.Generic;

namespace ManagerSoftware.GUI
{
    public partial class Manager_Software_Windows : Form
    {
        private Park engine;

       public Manager_Software_Windows()
        {
            InitializeComponent();
            this.engine = new Park();
        }

        private bool Check_Verification()
        {
            return (contactNumber.Text.Length > 0) && (email.Text.Length > 0) &&
                (name.Text.Length > 0) && (role.Text.Length > 0);
        }

        private void get_park_number(object sender, EventArgs e)
        {
            int parking_number = -1;
            if (role.Text.Equals("Company"))
            {
                parking_number = engine.InsertQueue(name.Text, email.Text, contactNumber.Text, company_name.Text);
            }
            else if (role.Text.Equals("Visitor"))
            {
                parking_number = engine.InsertStack(name.Text, email.Text, contactNumber.Text);
            }

            spot_number.Text = parking_number.ToString();
        }

        private void textChanged(object sender, EventArgs e)
        {
            get_parking_number.Enabled = Check_Verification();
        }

        private void reset_button(object sender, EventArgs e)
        {
            engine.Reset();
            Utilities.ResetAllControls(this);
        }

        private void get_day_info_Click(object sender, EventArgs e)
        {
            List<Car> company_list = engine.GetCompanyOrderCars();
            List<Car> visitor_list = engine.GetVisitorsOrderCars();
           
            foreach (Car car in company_list)
            {
                companiesList.Items.Add(car.Id);
            }
            foreach (Car car in visitor_list)
            {
                visitorsList.Items.Add(car.Id);
            }

        }
    }
}